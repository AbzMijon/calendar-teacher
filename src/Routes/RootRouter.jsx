import React from 'react';
import { Routes, Route } from 'react-router-dom';
import { ROUTES } from '../constans/routes';
import TeacherProfile from '../Scenes/TeacherProfile';

function RootRoute() {
    return (
        <Routes>
            <Route path={ROUTES.homePage} element={<TeacherProfile />}></Route>
            <Route path='*' element={<h2 className='erorr--not-found'>Ресурс не найден!</h2>}></Route>
        </Routes>
    )
}

export default RootRoute;