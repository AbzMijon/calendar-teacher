import { DAYS } from "../constans/days";

export const range = (end) => {
    const maxDays = 35;
    const { result } = Array.from({ length: end /* + (35 - end) */}).reduce(
        ({ result, current }) => ({
            result: [...result, current],
            current: current + 1
        }),
        { result: [], current: 1 }
        );
    return result;
};

export const getDaysInMonth = (month, year) => {
    return new Date(year, month + 1, 0).getDate();
};

export const sortDays = (month, year) => {
    const dayIndex = new Date(year, month, 1).getDay();
    const sortedDays = [...DAYS.slice(dayIndex), ...DAYS.slice(0, dayIndex)];
    return sortedDays;
};

export const getSortedDays = (date, days) => {
    const daysInMonth = range(days);
    const index = new Date(date.getFullYear(), date.getMonth(), 1).getDay();
    return [...Array(index === 0 ? 6 : index - 1), ...daysInMonth];
};

export const getDateObj = (day, month, year) => {
    return new Date(year, month, day);
};

export const areDatesEqual = (firstDate, secondDate) => {
    return (
        firstDate.getFullYear() === secondDate.getFullYear() &&
        firstDate.getMonth() === secondDate.getMonth() &&
        firstDate.getDate() === secondDate.getDate()
    );
};