import React from 'react';
import styled from 'styled-components';
import { useAppDispatch } from '../hooks/reduxHooks';
import { setCheckedDay } from '../store/slices/WorkDaysSlice';
import { MdOutlineDone } from 'react-icons/md'; 

const StyledWorkDays = styled.div `
    .time__days {
        display: flex;
        justify-content: space-between;
        align-content: center;
        background-color: #fff;
        box-shadow: 0px 0px 20px rgba(18, 17, 39, 0.1);
        border-radius: 20px;
        padding: 23px 60px;
    }
    .time__day + .time__day {
        margin-left: 40px;
    }
    .time__day {
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .time__day-name {
        margin-left: 10px;
        font-size: 16px;
        display: flex;
        line-height: 19px;
        color: #3C4852;
        input {
            display: none;
        }
    }
    .time__fake {
        margin-right: 10px;
        display: inline-block;
        border-radius: 50px;
        width: 20px;
        height: 20px;
        color: #fff;
        font-size: 15px;
        border: 1px solid #3C4852;
        position: relative;
        &:before {
            content: '';
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            display: block;
            width: 100%;
            height: 100%;
            background-color: #FDCE0E;
            border-radius: 50%;
            opacity: 0;
            transition: 0.2s all;
        }
    }
    .time__input-radio:checked + .time__fake:before {
        opacity: 1;
    }
    .time__fake--active {
        border: none;
    }
    .time__fake-icon {
        color: #fff;
        width: 100%;
        height: 100%;
        border-radius: 50%;
        font-size: 13px;
        font-weight: bold;
        background-color: #FDCE0E;
        z-index: 6;
        position: relative;
    }
    .time__fake-icon--unactive {
        background-color: transparent;
        color: transparent;
    }
`

function WorkDays({ workDays }) {

    const dispatch = useAppDispatch();

    return (
        <StyledWorkDays>
            <ul className="time__days">
                {workDays.map((day) => {
                    return (
                        <li key={day.id} className='time__day'>
                            <label className="time__day-name">
                                <input type="radio" onClick={() => dispatch(setCheckedDay(day.id))} />
                                <span className={day.isChecked ? 'time__fake time__fake--active' : 'time__fake'}><MdOutlineDone className={day.isChecked ? 'time__fake-icon' : 'time__fake-icon--unactive'}/></span>
                                <span className='time__text'>{day.id}</span>
                            </label>
                        </li>
                    )
                })}
            </ul>
        </StyledWorkDays>
    )
}

export default WorkDays;