import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { GrFormPrevious, GrFormNext } from 'react-icons/gr';
import { MONTHS } from '../constans/days';
import { getDateObj, getDaysInMonth, getSortedDays, getSortedWeekDays, range } from '../helpers/calendar';
import { MdKeyboardArrowUp, MdKeyboardArrowDown } from 'react-icons/md';
import { useAppSelector, useAppDispatch } from '../hooks/reduxHooks';
import { 
    clearAllCheck, 
    setArrOfDays, 
    setDays, 
    toggleNotFilledCheck, 
    setPrevMonth, 
    setNextMonth, 
    setFirstMonth, 
    setLastMonth, 
    setNextYear, 
    setPrevYear,
    filledSelectedDays,
} from '../store/slices/DaysSlice';
import { DAYS } from '../constans/days';

const StyledCalendar = styled.div `
    .calendar {
        color: #000;
        margin: 30px 0;
    }
    .calendar__body {
        position: relative;
    }
    .calendar__header {
        width: 100%;
        display: flex;
        justify-content: center;
        align-content: center;
    }
    .calendar__nav-btn {
        font-size: 20px;
        color: #3C4852;
        border: none;
        outline: none;
        background-color: transparent;
        cursor: pointer;
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
    }
    .prev-btn {
        left: -30px;
    }
    .next-btn {
        right: -30px;
    }
    .calendar__date {
        font-size: 16px;
        line-height: 19px;
        color: #3C4852;
    }
    .calendar__week-days {
        position: relative;
        margin-top: 25px;
        display: grid;
        grid-template-columns: repeat(7, 1fr);
        width: 100%;
        color: #A9AEB2;
        justify-items: center;
        align-items: center;
    }
    .calendar__days {
        margin-top: 25px;
        display: grid;
        grid-template-columns: repeat(7, 1fr);
        justify-items: center;
        align-items: center;
        row-gap: 30px;
        width: 100%;
    }
    .calendar__week-day {
        font-size: 16px;
        line-height: 19px;
    }
    .calendar__day {
        cursor: pointer;
        transition: 0.1s all;
        padding: 8px 14px;
        border-radius: 50%;
        width: 55px;
        height: 55px;
        display: flex;
        border: none;
        outline: none;
        background-color: #fff;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        &:hover {
            color: #1414143b;
        }
    }
    .calendar__type-icon {
        margin-right: 10px;
    }
    .calendar__type {
        margin: 25px 0 0px 0;
        color: #3C4852;
        font-size: 16px;
        line-height: 19px;
        display: flex;
        justify-content: center;
        align-items: center;
        cursor: pointer;
    }
    .calendar__day--active {
        background-color: #FDCE0E;
    }
    .calendar-days__clear {
        position: absolute;
        bottom: 0px;
        right: 15px;
        background: #FDCE0E;
        border-radius: 50px;
        padding: 12px 17px;
        font-size: 17px;
        font-weight: bold;
        color: #fff;
        cursor: pointer;
        border: none;
        outline: none;
    }
    .calendar__day--filled {
        background-color: #1414143b;
        &:disabled {
            cursor: not-allowed;
        }
    }
    .calendar__day--disabled {
    }
    .calendar__day--filledCheck {
        border: 2px solid #FDCE0E;
    }
    .calendar__day--filledActive {
        border: 2px solid #FDCE0E;
    }
`

function Calendar({ eventsArr, addEvent, handleSell, setHandleSell }) {

    const countOfDays = useAppSelector(state => state.days.сountOfDays);
    const year = useAppSelector(state => state.days.currentYear);
    const month = useAppSelector(state => state.days.currentMonth);
    const days = useAppSelector(state => state.days.days);
    const selectedDays = useAppSelector(state => state.days.selectedDays);
    const dispatch = useAppDispatch();
    const DAYSINMONTH = getDaysInMonth(month, year);
    const [currentWeek, setCurrentWeek] = useState(1);
    const [fullCalendarType, setFullCalendarType] = useState(true);
    const filledDays = useAppSelector(state => state.days.filledDays);

    const firstWeek = [1, 2, 3, 4, 5, 6, 7];
    const secondWeek = [8, 9, 10, 11, 12, 13, 14];
    const thirdWeek = [15, 16, 17, 18, 19, 20, 21];
    const fouthWeek = [22, 23, 24, 25, 26, 27, 28];
    const fifthWeek = [];

    let lastEveryMonthDay = 28;
    for(let i = lastEveryMonthDay + 1; i <= DAYSINMONTH; i++) {
        fifthWeek.push(i);
    }

    useEffect(() => {
        if(fullCalendarType) {
            dispatch(setDays(DAYSINMONTH));
            dispatch(setArrOfDays({type: 'full', days: countOfDays }));
        }   else {
            dispatch(setDays(7));
            switch(currentWeek) {
                case 1:
                    dispatch(setArrOfDays({type: 'short', days: firstWeek}));
                    break;
                case 2:
                    dispatch(setArrOfDays({type: 'short', days: secondWeek}));
                    break;
                case 3:
                    dispatch(setArrOfDays({type: 'short', days: thirdWeek}));
                    break;
                case 4:
                    dispatch(setArrOfDays({type: 'short', days: fouthWeek}));
                    break;
                case 5: /* some problems */
                    dispatch(setArrOfDays({type: 'short', days: fifthWeek}));
                    break;
                default:
                    dispatch(setArrOfDays({type: 'short', days: firstWeek}));
            }
        }
    }, [month, year, countOfDays, fullCalendarType, currentWeek]);

/*     const onAddEvent = (date) => {
        addEvent(date);
    } */
    
    const handleNext = () => {
        if(fullCalendarType) {
            if(month < 11) {
                dispatch(setNextMonth());
            }   else {
                    dispatch(setFirstMonth());
                    dispatch(setNextYear());
            }
        }   else {
            if(fifthWeek.length === 0 && currentWeek === 4) {
                dispatch(setNextMonth());
                setCurrentWeek(1);
            }   else if(currentWeek < 5) {
                setCurrentWeek((prev) => prev + 1);
            }   else if(month < 11) {
                dispatch(setNextMonth());
                setCurrentWeek(1);
            }   else {
                    dispatch(setFirstMonth());
                    setCurrentWeek(1);
                    dispatch(setNextYear());
            }
        }
    }
    const handlePrev = () => {
        if(fullCalendarType) {
            if(month > 0) {
                dispatch(setPrevMonth());
            }   else {
                    dispatch(setLastMonth());
                    dispatch(setPrevYear());
            }
        }   else {
            if(fifthWeek.length === 0 && currentWeek === 5) {
                setCurrentWeek(4);
            }   else if(currentWeek > 1) {
                setCurrentWeek((prev) => prev - 1);
            }   else if(month > 0) {
                    dispatch(setPrevMonth());
                    setCurrentWeek(5);
            }   else {
                    dispatch(setPrevYear());
                    dispatch(setLastMonth());
                    setCurrentWeek(5);
            }
        }
    }

    const handleCheckIfFillCellDisabled = (lenght_of_selected_days, filled_days, picked_cell,  day) => {
        if (lenght_of_selected_days > 0) {
            return true;
        }
        return false;
    };


    return (
        <StyledCalendar>
            <div className='calendar'>
                <header className="calendar__header">
                    <h3 className="calendar__date">{MONTHS[month]} {year}</h3>
                </header>
                <main className='calendar__body'>
                    <ul className="calendar__week-days">
                    {DAYS.map((day) => (
                        <li key={day} className='calendar__week-day'>{day}</li>
                    ))}
                        <button type='button' className='calendar__nav-btn prev-btn' onClick={handlePrev}><GrFormPrevious /></button>
                        <button type='button' className='calendar__nav-btn next-btn' onClick={handleNext}><GrFormNext /></button>
                    </ul>
                    <ul className='calendar__days'>
                        {days.map((day, i) => {
                            return (
                                <div key={i}>
                                    {day.filled ?
                                        <button disabled={handleCheckIfFillCellDisabled(selectedDays.length, filledDays, handleSell, day)} type='button'
                                            className={
                                                day.filled &&  day.title - 1 === handleSell ? 'calendar__day calendar__day--filled calendar__day--filledActive' :
                                                day.filled ? 'calendar__day calendar__day--filled' :
                                                'calendar__day'
                                            }
                                            onClick={() => {
                                                    setHandleSell(day.title - 1);
                                                    dispatch(filledSelectedDays({title: day.title, filled: day.filled}));
                                                }}
                                            >
                                            <p className='calendar__num'>{day.title}</p>
                                        </button>
                                    :
                                        <button type='button'
                                            className={
                                                day.check ? 'calendar__day calendar__day--active' :
                                                'calendar__day'
                                            }
                                                onClick={() => {
                                                    dispatch(toggleNotFilledCheck(day.title));
                                                    setHandleSell(day.title - 1);
                                                    dispatch(filledSelectedDays({title: day.title, filled: day.filled}));
                                            }}
                                        >
                                            <p className='calendar__num'>{day.title}</p>
                                        </button>
                                    }
                                </div>
                            )
                            
                        })}
                    </ul>
                    <p
                    className='calendar__type'
                    onClick={() => setFullCalendarType(!fullCalendarType)}>
                        {fullCalendarType ? <MdKeyboardArrowUp className='calendar__type-icon'/> : <MdKeyboardArrowDown className='calendar__type-icon' />}
                        {fullCalendarType ? 'Short Calender' : 'Full Calender'}
                    </p>
                    <button className='calendar-days__clear' onClick={() => dispatch(clearAllCheck())}>Clear</button>
                </main>
            </div>
        </StyledCalendar>
    )
}

export default Calendar;