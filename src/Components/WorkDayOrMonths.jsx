import React from 'react';
import styled from 'styled-components';
import { useAppDispatch } from '../hooks/reduxHooks';
import { setCheckedDayOrMonth } from '../store/slices/EveryDaysOrMontsSlice';
import { MdOutlineDone } from 'react-icons/md'; 

const StyledWorkDayOrMonth = styled.div `
    .time__day-or-months {
        margin: 25px 0;
        padding: 23px 60px;
        display: flex;
        justify-content: start;
        align-items: center;
        width: 100%;
        box-shadow: 0px 0px 20px rgba(18, 17, 39, 0.1);
        border-radius: 20px;
    }
    .time__everyday {
        margin-right: 75px;
    }
    .time__everyday, .time__everymonth {
        label {
            margin-left: 10px;
            font-size: 16px;
            line-height: 19px;
            color: #3C4852;
        }
    }
    .time__input-radio {
        display: none;
    }
    .time__fake {
        margin-right: 10px;
        display: inline-block;
        border-radius: 50px;
        width: 20px;
        height: 20px;
        color: #fff;
        font-size: 15px;
        border: 1px solid #3C4852;
        position: relative;
        &:before {
            content: '';
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            display: block;
            width: 100%;
            height: 100%;
            border-radius: 50%;
            opacity: 0;
            transition: 0.2s all;
        }
    }
    .time__input-radio:checked + .time__fake:before {
        opacity: 1;
    }
    .time__fake--active {
        border: none;
    }
    .time__fake-icon {
        color: #fff;
        width: 100%;
        height: 100%;
        border-radius: 50%;
        font-size: 13px;
        font-weight: bold;
        background-color: #FDCE0E;
        z-index: 6;
        position: relative;
    }
    .time__fake-icon--unactive {
        background-color: transparent;
        color: transparent;
    }
`

function WorkDayOrMonth({ workDayOrMonth }) {

    const dispatch = useAppDispatch();

    return (
        <StyledWorkDayOrMonth>
            <div className="time__day-or-months">
                {workDayOrMonth.map((elem) => {
                    return (
                        <div key={elem.title} className="time__everyday">
                            <label className='time__label'>
                                <input name='select-interval' type="radio" className='time__input-radio' onChange={() => dispatch(setCheckedDayOrMonth(elem.title))} />
                                <span className={elem.isChecked ? 'time__fake time__fake--active' : 'time__fake'}><MdOutlineDone className={elem.isChecked ? 'time__fake-icon' : 'time__fake-icon--unactive'}/></span>
                                <span className='time__text'>{elem.title}</span>
                            </label>
                        </div>
                    )
                })}
            </div>
        </StyledWorkDayOrMonth>
    )
}

export default WorkDayOrMonth;