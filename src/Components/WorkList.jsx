import React, { useState } from 'react';
import styled from 'styled-components';
import { getDateObj } from '../helpers/calendar';
import { useAppDispatch, useAppSelector } from '../hooks/reduxHooks';
import { setAllTime, setTimeForNotFilledCell, setTimeForFilledCell, deleteAllTime } from '../store/slices/DaysSlice';

const StyledWorkList = styled.div `
    .time__select-all {
        background: #FDCE0E;
        border-radius: 50px;
        padding: 12px;
        cursor: pointer;
        display: flex;
        justify-content: center;
        align-items: center;
        border: none;
        outline: none;
        color: #fff;
        font-size: 20px;
        font-weight: bold;
        width: 175px;
        height: 35px;
        &:hover {
            background-color: #f1c40e;
        }
    }
    .time__list {
        display: flex;
        flex-wrap: wrap;
        justify-content: left;
        align-items: center;
        gap: 25px 30px;
        width: 100%;
        padding: 10px 60px;
    }
    .time__item {
        padding: 9px 14px;
        text-align: center;
        color: #3C4852;
        font-size: 16px;
        border-radius: 100px;
        background-color: transparent;
        transition: 0.1s all;
        cursor: pointer;
        &:hover {
            background-color: #E4E5E6;
        }
    }
    .time__item--active {
        background-color: #FDCE0E;
        &:hover {
            background-color: #f1c40e;
        }
    }
`

function WorkList({ handleSell }) {

    const dispatch = useAppDispatch();
    const days = useAppSelector(state => state.days.days);
    const year = useAppSelector(state => state.days.currentYear);
    const month = useAppSelector(state => state.days.currentMonth);
    const [isAllTime, setIsAllTime] = useState(false);
    const index = new Date(getDateObj(1, month, year).getFullYear(), getDateObj(1, month, year).getMonth(), 1).getDay();

    //Мы никогда не получим index = 6, т.к. мы не вызываем фнкцию с calendar.js которая при значении 0 возвращает 6

    return (
        <StyledWorkList>
            <ul className="time__list">
                {index === 0 ?
                    days[handleSell + 6]?.times.map((time) => {
                        console.log('it is 0');
                        return (
                            <li
                            key={time.title}
                            className={time.check === true ? 'time__item time__item--active' : 'time__item'}
                            onClick={() => {
                                    dispatch(days[handleSell + 6].filled
                                        ? setTimeForFilledCell({ day: handleSell, time: time.title })
                                        : setTimeForNotFilledCell({ day: handleSell, time: time.title }));
                                    }}
                                    >{time.title}</li>
                                    )
                                })
                    :
                        days[handleSell + index - 1]?.times.map((time) => {
                            console.log('not 0');
                            return (
                                <li
                                    key={time.title}
                                    className={time.check === true ? 'time__item time__item--active' : 'time__item'}
                                    onClick={() => {
                                        dispatch(days[handleSell + index - 1].filled
                                            ? setTimeForFilledCell({ day: handleSell, time: time.title })
                                            : setTimeForNotFilledCell({ day: handleSell, time: time.title }));
                                    }}
                                >{time.title}</li>
                            )
                        })
                }
                <button className='time__select-all' type='button' onClick={() => {
                    setIsAllTime(!isAllTime);
                    if(isAllTime === false) {
                        dispatch(setAllTime());
                    }   else {
                        dispatch(deleteAllTime());
                    }
                }}>Select All</button>
            </ul>
        </StyledWorkList>
    )
}

export default WorkList;