import React from 'react';
import styled from 'styled-components';

const StyledHeader = styled.div `
    .header {
        width: 100%;
        border: 1px solid #000;
        z-index: 3;
        height: 70px;
        display: flex;
        justify-content: space-between;
        align-items: center;
        padding: 25px 95px;
        margin-bottom: 15px;
    }
    .header__logo {
        cursor: pointer;
        transition: 0.1s;
        &:hover {
            transform: scale(1.1);
        }
    }
    .header__list {
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .header__item {
        font-weight: 400;
        font-size: 20px;
        line-height: 24px;
        cursor: pointer;
        transition: 0.1s;
        &:hover {
            transform: scale(1.07);
        }
    }
    .header__item + .header__item {
        margin-left: 45px;
    }
    .header__profile {
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .header__avatar {
        width: 50px;
        height: 50px;
        border-radius: 50%;
        background-color: #000;
        border: 2px solid #cccccc;
        margin-left: 10px;
    }
`

function Header() {
    return (
        <StyledHeader>
            <header className='header'>
                <h3 className='header__logo'>Logo</h3>
                <ul className="header__list">
                    <li className="header__item">Item</li>
                    <li className="header__item">Item</li>
                    <li className="header__item">Item</li>
                    <li className="header__item">Item</li>
                    <li className="header__item">Item</li>
                </ul>
                <div className="header__profile">
                    <h5 className="header__profile-name">name</h5>
                    <div className="header__avatar"></div>
                </div>
            </header>
        </StyledHeader>
    )
}

export default Header;