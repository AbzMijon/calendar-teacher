import React from 'react';
import styled from 'styled-components';

const StyledAside = styled.div `
    .aside {
        border: 1px solid #000;
        padding: 40px 15px;
    }
    .aside__list {
        text-align: center;
    }
    .aside__item {
        padding: 5px 10px;
        font-size: 20px;
        line-height: 24px;
        text-align: center;
        transition: 0.1s ease-in-out;
        border-bottom: 2px solid transparent;
        cursor: pointer;
        &:hover {
            border-color: #cccccc;
        }
    }
    .aside__item  + .aside__item {
        margin-top: 55px;
    }
`

function Aside() {
    return (
        <StyledAside>
            <aside className='aside'>
                <ul className="aside__list">
                    <li className="aside__item">Item</li>
                    <li className="aside__item">Item</li>
                    <li className="aside__item">Item</li>
                    <li className="aside__item">Item</li>
                    <li className="aside__item">Item</li>
                    <li className="aside__item">Item</li>
                    <li className="aside__item">Item</li>
                </ul>
            </aside>
        </StyledAside>
    )
}

export default Aside;