import axios from 'axios';
import React, { useState } from 'react';
import styled from 'styled-components';
import Header from '../Components/Header';
import Aside from '../Components/Aside';
import WorkList from '../Components/WorkList';
import WorkDays from '../Components/WorkDays';
import WorkDayOrMonth from '../Components/WorkDayOrMonths';
import { clearDayOrMonth } from '../store/slices/EveryDaysOrMontsSlice';
import { clearCheckedDay } from '../store/slices/WorkDaysSlice';
import { useAppDispatch, useAppSelector } from '../hooks/reduxHooks';
import Calendar from '../Components/Calendar';
import { MOCKAPPS } from '../constans/days';
import { setFilled, filledSelectedDays, setSelectedDays } from '../store/slices/DaysSlice';
import { range } from '../helpers/calendar';

const StyledTeacherProfile = styled.div `
    background-color: #fff;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    .profile {
        margin: 0 auto;
        padding: 0 10px;
        max-width: 1115px;
        display: grid;
        grid-template-columns: 272px 1fr;
        column-gap: 15px;
        align-items: flex-start;
    }
    .main {
        border: 1px solid #000;
    }
    .time {
        padding: 31px 0 40px 0;
        text-align: center;
        background-color: #fff;
        border: 1px solid #FFFFFF;
        box-shadow: 0px 3px 12px rgba(183, 183, 183, 0.3);
        backdrop-filter: blur(5px);
        border-radius: 20px;
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        margin-bottom: 30px;
    }
    .calendar {
        padding: 10px 25px;
    }
    .time__select {
        border: none;
        outline: none;
        margin-bottom: 30px;
        color: #3C4852;
        font-size: 16px;
        line-height: 19px;
        background-color: #fff;
    }
    .time__save {
        display: flex;
        justify-content: end;
        align-items: center;
        width: 100%;
        margin: 25px 0;
        padding: 10px;
    }
    .time__cancel-btn, .time__save-btn {
        border-radius: 50px;
        padding: 12px;
        cursor: pointer;
        display: flex;
        justify-content: center;
        align-items: center;
        border: none;
        outline: none;
        color: #3C4852;
        font-size: 20px;
        font-weight: bold;
        width: 155px;
        height: 45px;
        background-color: transparent;
        &:hover {
            background-color: #E4E5E6;
        }
    }
    .time__save-btn {
        background: #FDCE0E;
        margin-left: 10px;
        color: #fff;
        
        &:hover {
            background-color: #f1c40e;
        }
    }
    .time__select-all {
        background: #FDCE0E;
        border-radius: 50px;
        padding: 12px;
        cursor: pointer;
        display: flex;
        justify-content: center;
        align-items: center;
        border: none;
        outline: none;
        color: #fff;
        font-size: 20px;
        font-weight: bold;
        width: 175px;
        height: 35px;
        &:hover {
            background-color: #f1c40e;
        }
    }
    .time__list {
        display: flex;
        flex-wrap: wrap;
        justify-content: left;
        align-items: center;
        gap: 25px 30px;
        width: 100%;
        padding: 10px 60px;
    }
    .time__item {
        padding: 9px 14px;
        text-align: center;
        color: #3C4852;
        font-size: 16px;
        border-radius: 100px;
        background-color: transparent;
        transition: 0.1s all;
        cursor: pointer;
        &:hover {
            background-color: #E4E5E6;
        }
    }
    .time__item--active {
        background-color: #FDCE0E;
        &:hover {
            background-color: #f1c40e;
        }
    }
`

function TeacherProfile() {

    const workDays = useAppSelector((state) => state.workDay.workDays);
    const dispatch = useAppDispatch();
    const [events, setEvents] = useState(MOCKAPPS);
    const year = useAppSelector(state => state.days.currentYear);
    const month = useAppSelector(state => state.days.currentMonth);
    const days = useAppSelector(state => state.days.days);
    const [handleSell, setHandleSell] = useState(0);
    const selectedDays = useAppSelector(state => state.days.selectedDays);
    const addEvent = (date) => {
        setEvents((prev) => [...prev, {date, title: 'send'}])
    }

    const postDataToServer = () => {
        const findElems = days.filter((day) => day.check);
        findElems?.forEach((day) => {
            const isTimeCheck = day.times.filter((time) => {
                return time.check;
            });
            if(day.check && isTimeCheck.length) {
                dispatch(setFilled());
                dispatch(setSelectedDays([]));
                axios.post('http://adressName', {
                    year: year,
                    month: month,
                    days: days,
                    workDays: workDays,
                })
            }
        });
    }

    const handleCancel = () => {
        dispatch(clearDayOrMonth());
        dispatch(clearCheckedDay());
    }

    return (
        <StyledTeacherProfile>
            <Header/>
            <div className='profile'>
                <Aside/>
                <main className="main">
                    <section className='settings'>
                    </section>
                    <section className='calendar'>
                        <Calendar eventsArr={events} addEvent={addEvent} handleSell={handleSell} setHandleSell={setHandleSell} />
                    </section>
                    <section className='time'>
                        <select className='time__select' name="gmt" id="">
                            <option value="minsk">Europe/Minsk GMT +3:00</option>
                            <option value="minsk">Europe/Minsk GMT +3:00</option>
                            <option value="minsk">Europe/Minsk GMT +3:00</option>
                        </select>
                        <WorkList handleSell={handleSell}/>
                        {/*<WorkDayOrMonth workDayOrMonth={workDayOrMonth}/>*/}
                    </section>
                    <section className='everydays'>
                        <WorkDays workDays={workDays}/>
                    </section>
                    <div className="time__save">
                        <button className='time__cancel-btn' onClick={handleCancel}>Cancel</button>
                        <button className='time__save-btn' onClick={postDataToServer}>Save</button>
                    </div>
                </main>
            </div>
        </StyledTeacherProfile>
    )
}

export default TeacherProfile;