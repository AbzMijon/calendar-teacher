import { configureStore, combineReducers } from '@reduxjs/toolkit';
import { persistStore,
        persistReducer,
        FLUSH,
        REHYDRATE,
        PAUSE,
        PERSIST,
        PURGE,
        REGISTER,
        } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import hardSet from 'redux-persist/es/stateReconciler/hardSet';
import WorkDaysSlice from './slices/WorkDaysSlice';
import EveryDaysOrMontsSlice from './slices/EveryDaysOrMontsSlice';
import SelectedDaysSlice from './slices/DaysSlice'

const persistConfig = {
    key: 'root',
    storage,
    version: 1,
    stateReconciler: hardSet,
};

const rootReducer = combineReducers({
    workDay: WorkDaysSlice,
    workDayOrMonth: EveryDaysOrMontsSlice,
    days: SelectedDaysSlice,
})

const persistedReducer = persistReducer<any>(persistConfig, rootReducer);

const store = configureStore ({
    reducer: persistedReducer,
    middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
        serializableCheck: {
            ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
        },
    }),
})

// export const persistor = persistStore(store);
export default store;
export type RootState = ReturnType<typeof rootReducer>
export type AppDispatch = typeof store.dispatch;