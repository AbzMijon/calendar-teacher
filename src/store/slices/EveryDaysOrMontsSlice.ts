import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type DayOrMonth = {
    title: string
    isChecked: boolean,
}

type DayOrMonthState = {
    DayOrMonthArr: DayOrMonth[],
}

const initialState: DayOrMonthState = {
    DayOrMonthArr: [
        {
            title: 'EveryDay',
            isChecked: false,
        },
        {
            title: 'Every month',
            isChecked: false,
        },
    ]
}

const WorkDayOrMonthSlice = createSlice({
    name: 'workDayOrMonth',
    initialState,
    reducers: {
        setCheckedDayOrMonth(state, action: PayloadAction<number | string>) {
            const findDayOrMonthElem = state.DayOrMonthArr.find((elem) => {
                return elem.title === action.payload;
            })
            state.DayOrMonthArr.forEach((elem) => {
                return elem.isChecked = false;
            })
            if(findDayOrMonthElem) {
                findDayOrMonthElem.isChecked = !findDayOrMonthElem.isChecked;
            } 
        },
        clearDayOrMonth(state) {
            state.DayOrMonthArr.forEach((elem) => {
                return elem.isChecked = false;
            })
        },
    },
})

export const {setCheckedDayOrMonth, clearDayOrMonth} = WorkDayOrMonthSlice.actions;
export default WorkDayOrMonthSlice.reducer;