import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { getDateObj, getSortedDays, range } from '../../helpers/calendar';

const initialState = {
    сountOfDays: 31,
    days: [] as any[],
    currentMonth: new Date().getMonth(),
    currentYear: new Date().getFullYear(),
    selectedDays: [] as number[],
    filledDays: [] as any,
    calendar: {
        jan: [],
        feb: [],
        march: [],
        april: [],
        may: [],
        june: [],
        jul: [],
        aug: [],
        sep: [],
        oct: [],
        nov: [],
        dec: [],
    },
}

const DaysSlice = createSlice({
    name: 'Days',
    initialState,
    reducers: {
        toggleNotFilledCheck(state, action: PayloadAction<number | string>) {
            const checkedDay: any = state.days.find((day: any) => day.title === action.payload);
            if (checkedDay.check === true) {
                state.days[checkedDay.title - 1].times = state.days[checkedDay.title - 1].times.map((time: any) => {
                    return { ...time , check: false }
                });
            }
            if(checkedDay) {
                checkedDay.check = !checkedDay.check;
            }
        },
        clearAllCheck(state) {
            state.days.forEach((day:any) => {
                day.check = false;
            });
        },
        setDays(state, action) {
            state.сountOfDays = action.payload;
        },
        setArrOfDays(state, action) {
            if(action.payload.type === 'short') {
                state.days = action.payload.days.map((elem) => {
                    return {
                        title: elem, 
                        check: false, 
                        times: [
                            {title: '05:00', check: false},
                            {title: '06:00', check: false},
                            {title: '07:00', check: false},
                            {title: '08:00', check: false},
                            {title: '09:00', check: false},
                            {title: '10:00', check: false},
                            {title: '11:00', check: false},
                            {title: '12:00', check: false},
                            {title: '13:00', check: false},
                            {title: '14:00', check: false},
                            {title: '15:00', check: false},
                            {title: '16:00', check: false},
                            {title: '17:00', check: false},
                            {title: '18:00', check: false},
                            {title: '19:00', check: false},
                            {title: '20:00', check: false},
                            {title: '21:00', check: false},
                            {title: '22:00', check: false},
                            {title: '23:00', check: false},
                        ],
                        filled: false,
                    };
                })
            }   else {
                    state.days = getSortedDays(getDateObj(1, state.currentMonth, state.currentYear), action.payload.days).map((elem) => {
                        return {
                            title: elem, 
                            check: false, 
                            times: [
                                {title: '05:00', check: false},
                                {title: '06:00', check: false},
                                {title: '07:00', check: false},
                                {title: '08:00', check: false},
                                {title: '09:00', check: false},
                                {title: '10:00', check: false},
                                {title: '11:00', check: false},
                                {title: '12:00', check: false},
                                {title: '13:00', check: false},
                                {title: '14:00', check: false},
                                {title: '15:00', check: false},
                                {title: '16:00', check: false},
                                {title: '17:00', check: false},
                                {title: '18:00', check: false},
                                {title: '19:00', check: false},
                                {title: '20:00', check: false},
                                {title: '21:00', check: false},
                                {title: '22:00', check: false},
                                {title: '23:00', check: false},
                            ],
                            filled: false,
                        };
                    });
            }
        },
        setSelectedDays(state, action) {
            state.selectedDays = action.payload;
        },
        setNextMonth(state) {
            state.currentMonth = state.currentMonth + 1;
        },
        setPrevMonth(state) {
            state.currentMonth = state.currentMonth - 1;
        },
        setFirstMonth(state) {
            state.currentMonth = 0;
        },
        setLastMonth(state) {
            state.currentMonth = 11;
        },
        setNextYear(state) {
            state.currentYear = state.currentYear + 1;
        },
        setPrevYear(state) {
            state.currentYear = state.currentYear - 1;
        },
        setFilled(state) {
            state.days = state.days.map((day: any) => {
                for (let i = 0; i < state.selectedDays.length; i++) {
                    if (state.selectedDays[i] === day.title) {
                        return { ...day, filled: true, check: false }
                    };
                }
                return { ...day };
            })
            state.filledDays = [...state.filledDays, ...state.selectedDays];
            state.selectedDays = [];
        },
        filledSelectedDays: (state, action: PayloadAction<any>) => {
            if (state.selectedDays.includes(action.payload.title)) {
                state.selectedDays = state.selectedDays.filter((cur) => cur !== action.payload.title)
            } else {
                if(action.payload.filled === false) {
                    state.selectedDays.push(action.payload.title);
                }
            }
        },
        setTimeForNotFilledCell: (state:any, action:any) => {
            const index = new Date(getDateObj(1, state.currentMonth, state.currentYear).getFullYear(), getDateObj(1, state.currentMonth, state.currentYear).getMonth(), 1).getDay() - 1;
            console.log(index);
            for(let i = 0; i < state.selectedDays.length; i++) {
                if(index === -1) {
                    state.days[state.selectedDays[i] + 5].times = state.days[state.selectedDays[i] + 5].times.map((time: any) => {
    
                        if (time.title === action.payload.time) {
                            return {...time, check: !time.check};
                        }
    
                        return {...time};
                    });
                } else {
                    state.days[state.selectedDays[i] + index - 1].times = state.days[state.selectedDays[i] + index - 1].times.map((time: any) => {
    
                        if (time.title === action.payload.time) {
                            return {...time, check: !time.check};
                        }
    
                        return {...time};
                    });
                }
            };
        },
        setTimeForFilledCell: (state:any, action:any) => {
            const index = new Date(getDateObj(1, state.currentMonth, state.currentYear).getFullYear(), getDateObj(1, state.currentMonth, state.currentYear).getMonth(), 1).getDay() - 1;
            if(index === -1) {
                state.days[action.payload.day + 6].times = state.days[action.payload.day + 6].times.map((time: any) => {
                        
                    if (time.title === action.payload.time) {
                        return {...time, check: !time.check};
                    }
                    return {...time};
                });
            }   else {
                    state.days[action.payload.day + index].times = state.days[action.payload.day + index].times.map((time: any) => {
                        
                        if (time.title === action.payload.time) {
                            return {...time, check: !time.check};
                        }
                        return {...time};
                    });
            }
        },
        setAllTime(state, action) {
            const index = new Date(getDateObj(1, state.currentMonth, state.currentYear).getFullYear(), getDateObj(1, state.currentMonth, state.currentYear).getMonth(), 1).getDay() - 1;
            if(index === -1) {
                for(let i = 0; i < state.selectedDays.length; i++) {
                    state.days[state.selectedDays[i] + 5].times = state.days[state.selectedDays[i] + 5].times.map((time:any) => {
                        return {...time, check: true};
                    });
                };
                for(let i = 0; i < state.filledDays.length; i++) {
                    state.days[state.filledDays[i] - 1].times = state.days[state.filledDays[i] - 1].times.map((time:any) => {
                        return {...time, check: true};
                    });
                };
            }   else {
                    for(let i = 0; i < state.selectedDays.length; i++) {
                        state.days[state.selectedDays[i] + index - 1].times = state.days[state.selectedDays[i] + index - 1].times.map((time:any) => {
                            return {...time, check: true};
                        });
                    };
                    for(let i = 0; i < state.filledDays.length; i++) {
                        state.days[state.filledDays[i] - 1].times = state.days[state.filledDays[i] - 1].times.map((time:any) => {
                            return {...time, check: true};
                        });
                    };
            }
        },
        deleteAllTime(state, action) {
            const index = new Date(getDateObj(1, state.currentMonth, state.currentYear).getFullYear(), getDateObj(1, state.currentMonth, state.currentYear).getMonth(), 1).getDay() - 1;
            for(let i = 0; i < state.selectedDays.length; i++) {
                state.days[state.selectedDays[i] + index].times = state.days[state.selectedDays[i] + index].times.map((time:any) => {
                    return {...time, check: false};
                });
            };
            for(let i = 0; i < state.filledDays.length; i++) {
                state.days[state.filledDays[i] + index].times = state.days[state.filledDays[i] + index].times.map((time:any) => {
                    return {...time, check: false};
                });
            };
        },
    },
})

export const {
    toggleNotFilledCheck,
    clearAllCheck,
    setDays,
    setArrOfDays,
    setSelectedDays,
    setNextMonth,
    setPrevMonth,
    setFirstMonth,
    setLastMonth,
    setNextYear,
    setPrevYear,
    setTimeForNotFilledCell,
    setTimeForFilledCell,
    setFilled,
    filledSelectedDays,
    setAllTime,
    deleteAllTime,
} = DaysSlice.actions;
export default DaysSlice.reducer;