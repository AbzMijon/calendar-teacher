import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type WorkDay = {
    id: string,
    isChecked: boolean,
}

type WorkDayState = {
    workDays: WorkDay[],
}

const initialState: WorkDayState = {
    workDays:[
        {
            id: 'Mon',
            isChecked: false,
        },
        {
            id: 'Tue',
            isChecked: false,
        },
        {
            id: 'Wed',
            isChecked: false,
        },
        {
            id: 'Thu',
            isChecked: false,
        },
        {
            id: 'Fri',
            isChecked: false,
        },
        {
            id: 'Sat',
            isChecked: false,
        },
        {
            id: 'Sun',
            isChecked: false,
        },
    ]
}

const WorkDaysSlice = createSlice({
    name: 'workDay',
    initialState,
    reducers: {
        setCheckedDay(state, action: PayloadAction<number | string>) {
            const findDayElem = state.workDays.find((day) => {
                return day.id === action.payload;
            })
            if(findDayElem) {
                findDayElem.isChecked = !findDayElem.isChecked;
            } 
        },
        clearCheckedDay(state) {
            state.workDays.forEach((day) => {
                return day.isChecked = false;
            })
        },
    },
})

export const {setCheckedDay, clearCheckedDay} = WorkDaysSlice.actions;
export default WorkDaysSlice.reducer;